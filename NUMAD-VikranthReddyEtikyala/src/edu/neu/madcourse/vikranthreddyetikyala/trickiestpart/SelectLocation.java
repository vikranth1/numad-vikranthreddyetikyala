package edu.neu.madcourse.vikranthreddyetikyala.trickiestpart;



import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import edu.neu.madcourse.vikranthreddyetikyala.R;


import edu.neu.mobileclass.apis.KeyValueAPI;

import android.app.ListActivity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SelectLocation extends ListActivity {

	private List<String> locations = new ArrayList<String>();
	private List<String> locationNames = new ArrayList<String>();
	private Set<String> locationSet = new HashSet<String>();
	private String otherUsername;
	private String username;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        username = Manager.getUsername();
        
        if(username.equals("vik"))
        {
        	otherUsername = "ale"; //getIntent().getExtras().getString("username");	
        }
        else if(username.equals("ale"))
        {
        	otherUsername = "vik";
        }
        
        
        try
        {
        	String top = KeyValueAPI.get(S.TEAM, S.PASS, otherUsername+S.TOP);
        	Scanner scan = new Scanner(top);
        	
        	Scanner sca = new Scanner(top);
			while(sca.hasNext())
			{
				String lati = "";
				String longi ="";
				boolean divide = false;
				
				char[] latilongi = sca.next().toCharArray();
				
				for(int i=0;i<latilongi.length;i++)
				{
					if(latilongi[i] == '+')
						divide = true;
					else if(!divide)
						lati += latilongi[i];
					else if(divide)
						longi += latilongi[i];
				}
				
				locations.add(lati+"+"+longi);
				
			    locationNames.add(getLocationNames(lati, longi));	
			}
        	
        }
        catch(Exception e)
        {
        	
        }
        
        setListAdapter(new ArrayAdapter<String>(this, R.layout.hm_simple_row, locationNames));
                
    }
    
    
    public String getLocationNames(String latitude, String longitude) 
    {
    	Geocoder gcd = new Geocoder(this, Locale.getDefault());
    	List<Address> addresses;
    	try 
    	{
	    	addresses = gcd.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1);
	    	
	    	if (addresses.size() > 0) 
	    	{
	    		//locationSet.add((addresses.get(0).getAddressLine(0) + " " +addresses.get(0).getLocality()+", "+addresses.get(0).getAdminArea()).replace(" ", "_"));
	    		return (addresses.get(0).getAddressLine(0) + " " +addresses.get(0).getLocality()+", "+addresses.get(0).getAdminArea()).replace(" ", "_");
	    	} 
	        	
	    			
    	} 
    	catch (Exception e) 
    	{
	    	Toast.makeText(this, "Could not get location name", Toast.LENGTH_SHORT).show();
	    }
		
    	return null;
	}


	public void onListItemClick(ListView l, View v, int position, long id)
    {
		Manager.setLocation(locations.get(position));
		Manager.setOtherusername(otherUsername);
    	Intent intent = new Intent(SelectLocation.this,SendMessage.class);
    	Manager.setLocation(locations.get(position));
    	startActivity(intent);
    }
    	

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hm_select_location, menu);
        return true;
    }
}
