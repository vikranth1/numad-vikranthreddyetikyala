package edu.neu.madcourse.vikranthreddyetikyala.trickiestpart;

import android.os.Bundle;

import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

import edu.neu.madcourse.vikranthreddyetikyala.R;

public class HmAbout extends Activity {

	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hm_about);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hm_about, menu);
        return true;
    }
}
