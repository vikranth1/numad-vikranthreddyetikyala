package edu.neu.madcourse.vikranthreddyetikyala.trickiestpart;



import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import edu.neu.madcourse.vikranthreddyetikyala.R;
import edu.neu.mobileclass.apis.KeyValueAPI;

public class HmMenuScreen extends Activity implements LocationListener{

	private Button sendMessage;
	private Button about;
	private Button exit;
	private static String username;
	public static double latitude;  
    public static double longitude; 
    private static double prevLatitude;
    private LocationManager locationManager;
    private ProgressDialog progressDialouge;
    private Handler handler;
    private long time;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hm_menu_screen);
        
        Log.e("Hey",Environment.getExternalStorageDirectory().getAbsolutePath()+"");
       
        /*
        progressDialouge = new ProgressDialog(HmMenuScreen.this);
		progressDialouge.setMessage("Loading...");
		progressDialouge.setCancelable(false);
		progressDialouge.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialouge.show(); 
		*/
        handler = new Handler();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
         
        
         
          username = Manager.getUsername();
        
        sendMessage = (Button) findViewById(R.id.sendMessage);
        about = (Button) findViewById(R.id.trickyabout);
        exit = (Button) findViewById(R.id.hm_exit);
        
        sendMessage.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Intent intent = new Intent(HmMenuScreen.this, SelectLocation.class);
				startActivity(intent);
				
			}
		});
       
        about.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Intent intent = new Intent(HmMenuScreen.this,HmAbout.class);
				intent.putExtra("username", username);
				startActivity(intent);
				
			}
		});
        
        exit.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Intent intent = new Intent(HmMenuScreen.this, HmLoginScreen.class);
				startActivity(intent);
			}
		});
        
    }
    
    public void onStart()
    {
    	super.onStart();
    	
    	
    	time = System.currentTimeMillis();
    	
    	Thread t = new Thread(new Runnable() {
			
			public void run() {
				
				handler.post(new Runnable() {
					
					public void run() {
						// TODO Auto-generated method stub
						getLocation();				 
					}
				});
			  
										
				
				//progressDialouge.dismiss();
					
			}
		});
    	t.start();
    }
    
    public void getLocation()
    {	
    	locationManager.requestLocationUpdates("gps", 30000, 0, this);
    	Toast.makeText(this, "latitude: "+ latitude + "longitude: " + longitude, Toast.LENGTH_SHORT).show();
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hm_menu_screen, menu);
        return true;
    }
    

	public void onLocationChanged(Location location) 
	{
		latitude = location.getLatitude();
		longitude = location.getLongitude();
		executeLater();
	}

	
	public void executeLater()
	{

		String currentLocationName = "";
		if(latitude != 0 )
		{

			// gets your current location and adds it to your queue
			try
			{
				//Toast.makeText(HmMenuScreen.this, username+S.TOP, Toast.LENGTH_SHORT).show();
				String top = KeyValueAPI.get(S.TEAM, S.PASS, username+S.TOP); 
				currentLocationName =  latitude+"+"+longitude;//getLocationNames(latitude+"", longitude+"");
				
				Scanner sca = new Scanner(top);
				boolean adding = true;
				
				while(sca.hasNext())
				{
					String lati = "";
					String longi ="";
					boolean divide = false;
					
					char[] latilongi = sca.next().toCharArray();
					
					for(int i=0;i<latilongi.length;i++)
					{
						if(latilongi[i] == '+')
							divide = true;
						else if(!divide)
							lati += latilongi[i];
						else if(divide)
							longi += latilongi[i];
					}
					
					
					//Log.e("hello",);
					
					if(radius(Double.valueOf(lati), Double.valueOf(longi), latitude, longitude) < 0.2)
					{				
						adding = false;
				     }	
				}

				if(adding)
				{
					KeyValueAPI.put(S.TEAM, S.PASS, username+S.TOP, currentLocationName +" "+ top );
				}
				prevLatitude = latitude;
			}
			catch(Exception e)
			{
				Toast.makeText(HmMenuScreen.this, "Problem here in detection", Toast.LENGTH_SHORT).show();
			}
			
			
			//reads your notification queue and checks for any messages
			try
			{
			     String locations = KeyValueAPI.get(S.TEAM, S.PASS, username+S.LOCATION_QUEUE);	
			     
			     Scanner sca = new Scanner(locations);
			     int count = 0;
			     
				  while(sca.hasNext())
					{
					  
						String lati = "";
						String longi ="";
						boolean divide = false;
						
						char[] latilongi = sca.next().toCharArray();
						
						for(int i=0;i<latilongi.length;i++)
						{
							if(latilongi[i] == '+')
								divide = true;
							else if(!divide)
								lati += latilongi[i];
							else if(divide)
								longi += latilongi[i];
						 }
					
			    	
				  	 if(radius(latitude, longitude, Double.valueOf(lati), Double.valueOf(longi)) < 0.5)
			    	 {
			    		// Toast.makeText(HmMenuScreen.this, "helo from gotcha message", Toast.LENGTH_SHORT).show();
			    		 
			    		 String notificationMesage = KeyValueAPI.get(S.TEAM, S.PASS, username+S.MESSAGE_QUEUE);	

			    		 int newCount = 0; 
			    		 Scanner scanMessage = new Scanner(notificationMesage);
						 
			    		 while(scanMessage.hasNext())
			    		 {
			    			 if(newCount == count)
			    			 {
			    				 String message = scanMessage.next();
			    				 clearQueue(currentLocationName, message);
			    				 makeNotification(message.replace("_", " "));
			    				// Toast.makeText(HmMenuScreen.this, "returned", Toast.LENGTH_SHORT).show();
			    				 
			    				 break;
			    			 }
			    			 scanMessage.next();
			    			 newCount++;
			    		 }
			    		 break;
			    	 }
			    	 count++;
					}
			     }
			
			catch(Exception e)
			{
				Toast.makeText(HmMenuScreen.this, "Problem here in notific", Toast.LENGTH_SHORT).show();
			}
		
		}
	}
	
	   public double radius(double lat1, double lon1, double lat2, double lon2)
	    {
	    	double R = 6371; 
	    	double dLat = Math.toRadians(lat2-lat1);
	    	double dLon = Math.toRadians(lon2-lon1);
	    	lat1 = Math.toRadians(lat1);
	    	lat2 = Math.toRadians(lat2);

	    	double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	    	        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
	    	double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	    	
	    	return  R * c; 
	    }
	
	public void clearQueue(String currentLocation, String message)
	{
		//Toast.makeText(HmMenuScreen.this, "helo from cleaqueue", Toast.LENGTH_SHORT).show();
		try
		{
			String locationQueue = KeyValueAPI.get(S.TEAM, S.PASS, username+S.LOCATION_QUEUE);
			String notification = KeyValueAPI.get(S.TEAM, S.PASS, username+S.MESSAGE_QUEUE);
			
			locationQueue = locationQueue.replace(currentLocation, "");
			notification = notification.replace(message, "");
			
			KeyValueAPI.put(S.TEAM, S.PASS, username+S.LOCATION_QUEUE, locationQueue);
			KeyValueAPI.put(S.TEAM, S.PASS, username+S.MESSAGE_QUEUE, notification);
		}	
		catch(Exception e)
		{
			//Toast.makeText(MenuScreen.this, "No Internet", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	private void makeNotification(String next) {
		
		//Toast.makeText(HmMenuScreen.this, "helo from make notification", Toast.LENGTH_SHORT).show();
		
		String otherPlayer = "";
		if(username.equals("vik"))
			otherPlayer = "ale";
		else
			otherPlayer = "vik";
			
		Notification noti = new Notification(R.drawable.notification, "You have a Message!", System.currentTimeMillis());
		Intent intent = new Intent(HmMenuScreen.this,DisplayMessage.class);
		intent.putExtra("message","You have a message left from "+otherPlayer+" :"+ next);
		
		PendingIntent contentIntent = PendingIntent.getActivity(HmMenuScreen.this, 0, intent, 0);
		
		noti.setLatestEventInfo(getApplicationContext(), "You have a message from" + username , "", contentIntent);
		
		noti.flags = Notification.FLAG_AUTO_CANCEL;
		
		noti.defaults = Notification.DEFAULT_ALL;
	    
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(1, noti);
	}

	public String getLocationNames(String latitude, String longitude) 
    {
    	Geocoder gcd = new Geocoder(this, Locale.getDefault());
    	List<Address> addresses;
    	try 
    	{
	    	addresses = gcd.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1);
	    	
	    	if (addresses.size() > 0) 
	    	{
	    		Toast.makeText(this, addresses.get(0).getAddressLine(0) + "", Toast.LENGTH_SHORT).show();
	    		//locationNames.add(addresses.get(0).getAddressLine(0) + "");
	    		return (addresses.get(0).getAddressLine(0) + " " +addresses.get(0).getLocality()+", "+addresses.get(0).getAdminArea()).replace(" ", "_");
	    	}
	    		
	    			
    	} 
    	catch (Exception e) 
    	{
	    	Toast.makeText(this, "Could not get location name", Toast.LENGTH_SHORT).show();
	    }
    	return null;
		
	}

	
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
}
