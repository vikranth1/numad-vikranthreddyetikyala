package edu.neu.madcourse.vikranthreddyetikyala.trickiestpart;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

import edu.neu.madcourse.vikranthreddyetikyala.R;

public class DisplayMessage extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hm_display_message);
        
        String msg = getIntent().getExtras().getString("message");
        
        TextView message;
        
        message = (TextView)findViewById(R.id.displayMessage);
        
        message.setText(msg);
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hm_display_message, menu);
        return true;
    }
}
