package edu.neu.madcourse.vikranthreddyetikyala.trickiestpart;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import edu.neu.mobileclass.apis.KeyValueAPI;
import edu.neu.madcourse.vikranthreddyetikyala.R;


public class HmLoginScreen extends Activity {

	private EditText username;
	private EditText password;
	private Button submit;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hm_login_screen);
        
        username = (EditText) findViewById(R.id.userfield);
        password = (EditText) findViewById(R.id.passfield);
        submit = (Button) findViewById(R.id.submit);
        
        submit.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) 
			{	
				if(authenticate())
		        {
		             Intent intent = new Intent(HmLoginScreen.this, HmMenuScreen.class);
		             Manager.setUsername(username.getText().toString());
		             startActivity(intent);
		             
		        }
				else
				{
					Toast.makeText(HmLoginScreen.this, "Invalid Details", Toast.LENGTH_SHORT).show();
				}
			}
		});
        
    }
    
    public boolean authenticate()
    {
    	
    	if(username.toString().equals(" ") || username.equals(""))
    		return false;
    	
    	try
    	{
    		String users = "";
    		
    		
    		users  = KeyValueAPI.get(S.TEAM, S.PASS, S.USERS);
    		
    	    
    		Log.e("users", users+" hello");
    		Log.e("invalid", " "+username.getText().toString()+"+"+password.getText().toString());
    		
    		if(users.contains(" "+username.getText().toString()+"+"+password.getText().toString()))
    		{
    			return true;	
    		}
    	}
    	catch(Exception e)
    	{
            Toast.makeText(HmLoginScreen.this, "Sorry! No Internet", Toast.LENGTH_SHORT).show();   		
    	}
    	return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hm_login_screen, menu);
        return true;
    }
}
