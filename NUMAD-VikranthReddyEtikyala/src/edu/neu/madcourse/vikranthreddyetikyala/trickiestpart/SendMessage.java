package edu.neu.madcourse.vikranthreddyetikyala.trickiestpart;



import edu.neu.mobileclass.apis.KeyValueAPI;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import edu.neu.madcourse.vikranthreddyetikyala.R;


public class SendMessage extends Activity {
	
	private EditText text;
	private Button send;
	private String username;
	private String otherusername;
	private String location;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hm_send_message);
        
        username = Manager.getUsername();
        otherusername = Manager.getOtherusername();
        location = Manager.getLocation();
        
        text = (EditText) findViewById(R.id.message);
        send = (Button) findViewById(R.id.send);
        
        send.setOnClickListener(new OnClickListener() {
			 
			public void onClick(View v) {
				
				try
				{
					Toast.makeText(SendMessage.this, otherusername +" "+ location, Toast.LENGTH_SHORT).show();
					String queue = KeyValueAPI.get(S.TEAM, S.PASS, otherusername+S.LOCATION_QUEUE);
				    String notification = KeyValueAPI.get(S.TEAM, S.PASS, otherusername+S.MESSAGE_QUEUE);
				    
				    KeyValueAPI.put(S.TEAM, S.PASS, otherusername+S.LOCATION_QUEUE, queue +" "+ location);
				    
				  
	     			 KeyValueAPI.put(S.TEAM, S.PASS, otherusername+S.MESSAGE_QUEUE, notification + " " + text.getText().toString().replace(" ", "_"));
				    
				    
				     Toast.makeText(SendMessage.this, "Message Sent", Toast.LENGTH_SHORT).show();
				    
				    
				    Intent intent = new Intent(SendMessage.this, HmMenuScreen.class);
				    startActivity(intent);
				    
				    
				}
				catch(Exception e)
				{
					Toast.makeText(SendMessage.this, "Message Failed", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hm_send_message, menu);
        return true;
    }
}
