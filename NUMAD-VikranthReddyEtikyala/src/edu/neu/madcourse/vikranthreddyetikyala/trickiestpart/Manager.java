package edu.neu.madcourse.vikranthreddyetikyala.trickiestpart;

public class Manager {

	
	private static String username;
	private static String otherusername;
	private static String location;
	
	
	public static String getUsername() {
		return username;
	}
	public static void setUsername(String username) {
		Manager.username = username;
	}
	public static String getOtherusername() {
		return otherusername;
	}
	public static void setOtherusername(String otherusername) {
		Manager.otherusername = otherusername;
	}
	public static String getLocation() {
		return location;
	}
	public static void setLocation(String location) {
		Manager.location = location;
	}
	
	

}

