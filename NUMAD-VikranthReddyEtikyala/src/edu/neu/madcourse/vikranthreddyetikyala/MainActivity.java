package edu.neu.madcourse.vikranthreddyetikyala;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import edu.neu.madcourse.vikranthreddyetikyala.boggle.Boggle;
import edu.neu.madcourse.vikranthreddyetikyala.persistentboggle.BMainActivity;
import edu.neu.madcourse.vikranthreddyetikyala.sudoku.Sudoku;
import edu.neu.madcourse.vikranthreddyetikyala.trickiestpart.HmLoginScreen;
import edu.neu.mobileClass.PhoneCheckAPI;

public class MainActivity extends Activity {

	private Button buttonTeamMembers;
	private Button buttonSudoku;
	private Button buttonCreateError;
	private Button buttonQuit;
	private Button buttonBoggle;
	private Button buttonPersistent;
	private Button buttonTrickiest;
	private Button buttonhardestPart;
	private final String name = "Name: Vikranth, Sudha";
	private final String email = "Email: vikranth@ccs.neu.com, nanduri.s@husky.neu.edu";
	private final String versionNum = "Version Number: ";
	private final String phoneId = "Phone Id: ";
	final Context context = this;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		

		if (!PhoneCheckAPI.doAuthorization(this)) {
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								context);
						alertDialogBuilder.setMessage(
								"This is a test application not intended for public use")
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
			
											public void onClick(DialogInterface dialog,
													int which) {
												finish();
			
											}
			
										});
						AlertDialog alertDialog = alertDialogBuilder.create();
						alertDialog.show();
			
					}
		setContentView(R.layout.activity_vikranth);

		buttonTeamMembers = (Button) findViewById(R.id.buttonTeamMembers);
		buttonSudoku = (Button) findViewById(R.id.buttonSudoku);
		buttonCreateError = (Button) findViewById(R.id.buttonCreateError);
		buttonQuit = (Button) findViewById(R.id.buttonQuit);
		buttonBoggle = (Button) findViewById(R.id.buttonBoggle);
		buttonPersistent = (Button) findViewById(R.id.buttonPersistentBoggle);
		buttonTrickiest = (Button) findViewById(R.id.buttonTrickiest);
        buttonhardestPart = (Button) findViewById(R.id.buttonHardest);
		
		buttonTeamMembers.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);

				try {
					String versionName = context.getPackageManager()
							.getPackageInfo(context.getPackageName(), 0).versionName;

					TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
					String android_id = telephonyManager.getDeviceId();

					alertDialogBuilder
							.setMessage(
									name + "\n" + email + "\n" + versionNum
											+ versionName + "\n" + phoneId
											+ android_id).setPositiveButton(
									"OK",
									new DialogInterface.OnClickListener() {

										public void onClick(
												DialogInterface dialog,
												int which) {

										}
									});
					AlertDialog alertDialog = alertDialogBuilder.create();

					alertDialog.show();
				} catch (NameNotFoundException e) {
					e.printStackTrace();
				}
			}
		});

		buttonSudoku.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, Sudoku.class);
				startActivity(intent);

			}
		});

		buttonBoggle.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, Boggle.class);
				startActivity(intent);

			}
		});
		
		buttonPersistent.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, BMainActivity.class);
				startActivity(intent);
				
			}
		});
		
		buttonTrickiest.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, HmLoginScreen.class);
				startActivity(intent);
				
			}
		});
		
		buttonhardestPart.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Intent intent = new Intent(MainActivity.this,ProjectMain.class);
				startActivity(intent);
				
			}
		});
		
		buttonCreateError.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				throw new Error();

			}
		});

		buttonQuit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_vikranth, menu);
		return true;
	}
}
