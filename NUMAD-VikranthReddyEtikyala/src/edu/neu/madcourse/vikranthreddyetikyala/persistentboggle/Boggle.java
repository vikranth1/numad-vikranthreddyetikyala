package edu.neu.madcourse.vikranthreddyetikyala.persistentboggle;

import android.os.Bundle;
import edu.neu.madcourse.vikranthreddyetikyala.R;

import android.app.Activity;
import android.view.Menu;

public class Boggle extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boggle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.boggle, menu);
        return true;
    }
}
