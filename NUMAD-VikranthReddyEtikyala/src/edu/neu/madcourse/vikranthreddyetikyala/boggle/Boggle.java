package edu.neu.madcourse.vikranthreddyetikyala.boggle;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import edu.neu.madcourse.vikranthreddyetikyala.R;

public class Boggle extends Activity {

	private Button newGame;
	private Button about;
	private Button acknowledgements;
	private Button quit;
	private Context context= this;
	private String OK = "OK";
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boggle);
        
        newGame = (Button) findViewById(R.id.newGame);
        about = (Button) findViewById(R.id.about);
        acknowledgements = (Button) findViewById(R.id.acknowledgements);
        quit = (Button) findViewById(R.id.buttonQuit);
        
        newGame.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				startActivity(new Intent(Boggle.this, BoggleGame.class));
				
			}
		});
        
        about.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				startActivity(new Intent(Boggle.this, About.class));
			}
		});
        
        acknowledgements.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
    					context);
    			alertDialogBuilder.setMessage(
    					"Boggle idea:Fun with words.com, Image Credits: Google Images Sound Credits: partnersinrhyme.com" )
    					.setPositiveButton(OK,
    							new DialogInterface.OnClickListener() {

    								public void onClick(DialogInterface dialog,
    										int which) {
    										

    								}

    							});
    			AlertDialog alertDialog = alertDialogBuilder.create();
    			alertDialog.show();
    			alertDialog.setCanceledOnTouchOutside(isRestricted());
			}
		});
        
        quit.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				finish();
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_screen, menu);
        return true;
    }
}
