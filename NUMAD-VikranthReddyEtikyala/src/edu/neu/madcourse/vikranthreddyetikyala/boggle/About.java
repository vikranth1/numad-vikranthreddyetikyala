package edu.neu.madcourse.vikranthreddyetikyala.boggle;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import edu.neu.madcourse.vikranthreddyetikyala.R;

public class About extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boggle_about);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.boggle_about, menu);
        return true;
    }
}
