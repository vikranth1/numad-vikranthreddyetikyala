package edu.neu.madcourse.vikranthreddyetikyala.boggle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

public class ButtonAdapater extends BaseAdapter {
	
	final String[] boggleStrings = {"AAEEGN",
								    "ELRTTY",
									"AOOTTW",
									"ABBJOO",
									"EHRTVW",
									"CIMOTV",
									"DISTTY",
									"EIOSST",
									"DELRVY",
									"ACHOPS",
									"HIMNQP",
									"EEINSU",
									"EEGHNW",
									"AFFKPS",
									"HLNNRZ",
									"DEILRX" };
	private char[] boggleAlphabets;
	private Context context;
	private int id = 0;
	private final int TILES_NUM = 16;
	
	public List<Button> getList() {
		return list;
	}

	public void setList(List<Button> list) {
		this.list = list;
	}

	private List<Button> list =  new ArrayList<Button>();
	public ButtonAdapater(Context context)
	{
		this.context = context;
		this.boggleAlphabets = generateChars();
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return 16;
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		if(list == null)
		  return null;
		else
		{
			return list.get(arg0);
		}
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		Button button;
		if (convertView == null) {  
            button = new Button(context);
            button.setFocusable(false);
    		button.setClickable(false);
    		Log.e("from if", ""+position);
        } else {
        	Log.e("from else", ""+position);
 
            return convertView;
        }
		button.setText((boggleAlphabets[position]=='Q')?(boggleAlphabets[position]+"u"):(boggleAlphabets[position]+""));
        
		button.setId(id++);
		list.add(button);
        return button;
	}
	
	public char[] generateChars()
    {
    	Collections.shuffle(Arrays.asList(boggleStrings));
	
    	char[] boggleAlphabets = new char[TILES_NUM];
		for(int i=0;i<TILES_NUM;i++)
		{
			int num = (int) (Math.random() * 5);
			boggleAlphabets[i] = boggleStrings[i].charAt(num);
			
		}
		
		return boggleAlphabets;
    }
	
}
