package edu.neu.madcourse.vikranthreddyetikyala.boggle;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import edu.neu.madcourse.vikranthreddyetikyala.R;

public class BoggleGame extends Activity {

	private TextView timer;
	private TextView words;
	private TextView scoreBoard;
	private TextView showWord;
	private List<String> allWords = new ArrayList<String>();
	private List<Integer> ids = new ArrayList<Integer>();
	private List<Integer> boggleManager = new ArrayList<Integer>();
	private List<Button> buttonsList = new ArrayList<Button>();
	private String currentWord = "";
	private Button addWord;
	private ImageView pause;
	private CountDownTimer cdTimer;
	private int score;
	private Context context = this;
	private long timeCounter = 120000;

	private final int TILES_NUM = 16;
	private final int MIN_LETTERS = 3;
	private final String RESUME = "Resume";
	private final String TXT = ".txt";
	private final String SCORE = "Score: ";
	private final String NO_SUCH_WORD = "No such Word";
	private final String INVALID_WORD = "Invalid Word";
	private final String WORD_ALREADY_IN_USE = "Word Already in Use";
	private final String MAIN_MENU = "Menu";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.boggle_game);

		timer = (TextView) findViewById(R.id.timer);
		words = (TextView) findViewById(R.id.allWords);
		scoreBoard = (TextView) findViewById(R.id.score);
		showWord = (TextView) findViewById(R.id.word);
		addWord = (Button) findViewById(R.id.addword);
		pause = (ImageView) findViewById(R.id.pause);

		onPause();

		addNewWord();

		timer();

		GridView gridView = (GridView) findViewById(R.id.gridview);
		gridView.setAdapter(new ButtonAdapater(this));

		gridView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				vib.vibrate(300);
				Button button = (Button) parent.getItemAtPosition(position);
				if ((boggleManager.contains(position) && !ids.contains(button.getId())) || boggleManager.isEmpty()) {
					
					ids.add(button.getId());
					currentWord += button.getText();
					showWord.setText(currentWord);

					for (int i = 0; i < TILES_NUM; i++) {
						Button b = (Button) parent.getItemAtPosition(i);
						b.setEnabled(true);
						boggleManager.clear();
						if (buttonsList.size() <= TILES_NUM)
							buttonsList.add(b);
					}
					boggleManager = getAdjacents(button.getId());
					for (int i = 0; i < TILES_NUM; i++) {
						if (!boggleManager.contains(i) || ids.contains(i)) {
							Button b = (Button) parent.getItemAtPosition(i);
							b.setEnabled(false);
						}

					}
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.boggle_game, menu);
		return true;
	}

	public void onPause() {
		super.onPause();
		pause.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				cdTimer.cancel();
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				alertDialogBuilder.setPositiveButton(RESUME,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								timer();

							}

						});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
				alertDialog.setCanceledOnTouchOutside(isRestricted());
			}
		});
	}

	public void addNewWord() {
		addWord.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				boolean check;
				try {
					if ((currentWord.length() >= MIN_LETTERS) && (currentWord.length() <= TILES_NUM)) {
						if (allWords.contains(currentWord)) {
							callToast(WORD_ALREADY_IN_USE);
							showWord.setText("");
						} else if (check = checkWord()
								&& !allWords.contains(currentWord)) {
							MediaPlayer mp = MediaPlayer.create(context,
									R.raw.boggle_claps);
							mp.start();
							words.setText(words.getText() + " " + currentWord
									+ "(" + (currentWord.length() - 2) + ")");
							score += currentWord.length() - 2;
							scoreBoard.setText("Score: " + score);
							showWord.setText("");
							allWords.add(currentWord);
						}

						else if (!check) {
							callToast(NO_SUCH_WORD);
							showWord.setText("");
						}
						currentWord = "";
					} else {
						currentWord = "";
						showWord.setText("");
						callToast(INVALID_WORD);
					}
					boggleManager.clear();
					ids.clear();
					for (Button button : buttonsList)
						button.setEnabled(true);

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		});
	}

	public void timer() {
		cdTimer = (new CountDownTimer(timeCounter, 1000) {

			public void onTick(long millisUntilFinished) {
				timeCounter = millisUntilFinished;
				timer.setText("Time: " + millisUntilFinished / 1000);
			}

			public void onFinish() {

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				alertDialogBuilder
						.setMessage(SCORE + score)
						.setPositiveButton(MAIN_MENU,
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int which) {
										finish();

									}

								});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
				alertDialog.setCanceledOnTouchOutside(isRestricted());

			}
		}.start());
	}

	public boolean checkWord() throws Exception {
		AssetManager assetManager = getAssets();
		InputStream ims = null;

		ims = assetManager.open(currentWord.length() + TXT);

		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(ims));
		String strLine;
		while ((strLine = bufferedReader.readLine()) != null) {
			if (strLine.equalsIgnoreCase(currentWord)) {
				bufferedReader.close();
				return true;
			}

		}
		bufferedReader.close();
		return false;
	}

	public void callToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	public List<Integer> getAdjacents(int id) {
		switch (id) {
		case 0:
			boggleManager.add(1);
			boggleManager.add(4);
			boggleManager.add(5);
			break;
		case 1:
			boggleManager.add(0);
			boggleManager.add(2);
			boggleManager.add(4);
			boggleManager.add(5);
			boggleManager.add(6);
			break;
		case 2:
			boggleManager.add(1);
			boggleManager.add(3);
			boggleManager.add(5);
			boggleManager.add(6);
			boggleManager.add(7);
			break;
		case 3:
			boggleManager.add(2);
			boggleManager.add(7);
			boggleManager.add(6);
			break;
		case 4:
			boggleManager.add(0);
			boggleManager.add(1);
			boggleManager.add(5);
			boggleManager.add(8);
			boggleManager.add(9);
			break;
		case 5:
			boggleManager.add(0);
			boggleManager.add(1);
			boggleManager.add(2);
			boggleManager.add(4);
			boggleManager.add(6);
			boggleManager.add(8);
			boggleManager.add(9);
			boggleManager.add(10);
			break;
		case 6:
			boggleManager.add(1);
			boggleManager.add(2);
			boggleManager.add(3);
			boggleManager.add(5);
			boggleManager.add(7);
			boggleManager.add(9);
			boggleManager.add(10);
			boggleManager.add(11);
			break;
		case 7:
			boggleManager.add(2);
			boggleManager.add(3);
			boggleManager.add(6);
			boggleManager.add(10);
			boggleManager.add(11);
			break;
		case 8:
			boggleManager.add(4);
			boggleManager.add(5);
			boggleManager.add(9);
			boggleManager.add(12);
			boggleManager.add(13);
			break;
		case 9:
			boggleManager.add(4);
			boggleManager.add(5);
			boggleManager.add(6);
			boggleManager.add(8);
			boggleManager.add(10);
			boggleManager.add(12);
			boggleManager.add(13);
			boggleManager.add(14);
			break;
		case 10:
			boggleManager.add(5);
			boggleManager.add(6);
			boggleManager.add(7);
			boggleManager.add(9);
			boggleManager.add(11);
			boggleManager.add(13);
			boggleManager.add(14);
			boggleManager.add(15);
			break;
		case 11:
			boggleManager.add(6);
			boggleManager.add(7);
			boggleManager.add(10);
			boggleManager.add(14);
			boggleManager.add(15);
			break;
		case 12:
			boggleManager.add(8);
			boggleManager.add(9);
			boggleManager.add(13);
			break;
		case 13:
			boggleManager.add(8);
			boggleManager.add(9);
			boggleManager.add(10);
			boggleManager.add(12);
			boggleManager.add(14);
			break;
		case 14:
			boggleManager.add(9);
			boggleManager.add(10);
			boggleManager.add(11);
			boggleManager.add(13);
			boggleManager.add(15);
			break;
		case 15:
			boggleManager.add(10);
			boggleManager.add(11);
			boggleManager.add(14);
			break;

		default:
			break;
		}

		return boggleManager;
	}
}
