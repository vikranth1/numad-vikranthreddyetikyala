package com.example.umessenger;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddFriend extends Activity {

	private EditText useridtext;
	private Button invite;
	private String username;
	private String userid;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_add_friend);
        
        useridtext = ((EditText) findViewById(R.id.friendid));
        invite = (Button) findViewById(R.id.button_invite);
        username = getIntent().getExtras().getString("username");
        
        invite.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				userid = useridtext.getText().toString();
				try
		        {
		        	String friends = KeyValueAPI.get(S.TEAM, S.PASS, username+S.FRIENDS);
		        	String incomingOther = KeyValueAPI.get(S.TEAM, S.PASS, userid+S.IN_REQUESTS);
		        	String incoming = KeyValueAPI.get(S.TEAM, S.PASS, username+S.IN_REQUESTS);
		        	String outgoing = KeyValueAPI.get(S.TEAM, S.PASS, username+S.OUT_REQUESTS);
		        	String userrecords = KeyValueAPI.get(S.TEAM, S.PASS, S.USER_RECORDS);
		        	
		        	if(userid.contains(" ")  || !(userrecords.contains(" "+userid+"+")))//|| userid.length() < 3)
		        	{
		        		alertBuilder("Invalid id");
		        	}
		        	else if(incomingOther.contains(" "+userid+" "))
		        	{
		        		alertBuilder("Request pending for "+userid);
		        	}
		        	else if(outgoing.contains(" "+userid+" "))
		        	{
		        		alertBuilder("You have already sent a request to "+userid);
		        	}
		        	else if(friends.contains(" "+userid+" "))
		        	{
		        		alertBuilder(userid+" is your friend");
		        	}
		        	else
		        	{
		        		
		        		KeyValueAPI.put(S.TEAM, S.PASS, userid+S.IN_REQUESTS, incomingOther+" "+username);
		        		KeyValueAPI.put(S.TEAM, S.PASS, username+S.OUT_REQUESTS, outgoing+" "+userid);
		        		Toast.makeText(AddFriend.this, "Friend Request Sent to "+userid, Toast.LENGTH_SHORT).show();
		        		finish();
		        	}
		        }
		        catch(Exception e)
		        {
		        	
		        }
			}
		});
        
        
    }

    
    public void alertBuilder(String message)
    {
    	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddFriend.this);

		alertDialogBuilder
				.setMessage(message
						).setPositiveButton(
						"OK",
						new DialogInterface.OnClickListener() {

							public void onClick(
									DialogInterface dialog,
									int which) {

							}
						});
		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();	
	
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.u_add_friend, menu);
        return true;
    }
}
