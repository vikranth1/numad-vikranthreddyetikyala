package com.example.umessenger;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import edu.neu.mobileclass.apis.HttpDownloader;
import edu.neu.mobileclass.apis.KeyValueAPI;
import edu.neu.mobileclass.apis.UploadAPI;

public class MenuScreen extends Activity implements LocationListener{

	private Button friends;
	private Button messages;
	private String username;
	private ProgressDialog progressDialog;
	private Handler handler;
	private static  double latitude;
	private static double longitude;
	private static double prevLatitude;
	private long time;
	private LocationManager locationManager;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_menu_screen);
        
        username = getIntent().getExtras().getString("username");
        handler = new Handler();
        friends = (Button) findViewById(R.id.button_friends);
        messages = (Button) findViewById(R.id.button_messages);
        
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        
        progressDialog = new ProgressDialog(MenuScreen.this);
		progressDialog.setMessage("Searching for GPS...");
		progressDialog.setCancelable(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.show(); 
		progressDialog.dismiss();
		
		
		friends.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Intent intent = new Intent(MenuScreen.this,FriendMenu.class);
				intent.putExtra("username", username);
				startActivity(intent);
				
			}
		});
		
		messages.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Intent intent = new Intent(MenuScreen.this, MessageMenu.class);
				intent.putExtra("username", username);
				startActivity(intent);
				
			}
		});
    }

    
    public void onStart()
    {
        super.onStart();
        
        time = System.currentTimeMillis();
        Thread thread = new Thread(new Runnable() {
			
			public void run() {
						
                  handler.post(new Runnable() {
					
					public void run() {
						// TODO Auto-generated method stub
						 getLocation();
					}
				});
			  
            
				while((latitude == 0) || (latitude == prevLatitude))
				{
					
					if((System.currentTimeMillis() - time) > 60000)
					{
						
					   handler.post(new Runnable() {
						
						public void run() {
							
							progressDialog.dismiss();
							
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
									MenuScreen.this);

								alertDialogBuilder
										.setMessage("GPS Unavailable, the cloud may obstruct the signal"
												).setPositiveButton(
												"OK",
												new DialogInterface.OnClickListener() {

													public void onClick(
															DialogInterface dialog,
															int which) {

													}
												});
								AlertDialog alertDialog = alertDialogBuilder.create();

								alertDialog.show();	
								
						}
					});	
						
						break;
					}
						
				}
				progressDialog.dismiss();
			}
		});
        
        thread.start();
    }
    
    public void getLocation()
    {
    	locationManager.requestLocationUpdates("gps", 60000, 0, this);
    }
    
    public void onLocationChanged(Location location) 
	{
		latitude = location.getLatitude();
		longitude = location.getLongitude();
		addRecentLocation();
		detectMessages();
		directNotifications();
	}
    
    public void addRecentLocation()
    {
    	
    	if(latitude != 0 && latitude != prevLatitude)
    	{
    		
    		prevLatitude = latitude;
	    	String currentLat = "";
	    	String currentLon = "";
	    	
	    	//scan through the list and check if the new location th user is in a duplicate location in his top 10
	    	try
	    	{
	    		String recent = KeyValueAPI.get(S.TEAM, S.PASS, username+S.RECENT);
	            Scanner scan = new Scanner(recent);
	    		
	            if(!scan.hasNext())
	            {
	            	KeyValueAPI.put(S.TEAM, S.PASS, username+S.RECENT,latitude+"+"+longitude+"");
	            	return;
	            }
	    		while(scan.hasNext())
	    		{
	    			char[] latlong = scan.next().toCharArray();
	    			boolean divide = false;
	    			for(int i = 0; i < latlong.length; i++)
	    			{
	    				if(latlong[i] == '+')
	    				{
	    					divide = true;
	    				}
	    				else if(!divide)
	    				{
	    					currentLat += latlong[i];
	    				}
	    				else if(divide)
	    				{
	    					currentLon += latlong[i];
	    				}
	    			}
	    			
	    			//If the location is already in the list then return as this is a duplicate location
	    			if(radius(latitude, longitude, Double.valueOf(currentLat), Double.valueOf(currentLon)) < 0.2)
	    			{
	    				Toast.makeText(MenuScreen.this, radius(latitude, longitude, Double.valueOf(currentLat), Double.valueOf(currentLon))+"", Toast.LENGTH_SHORT).show();
	    				return;	
	    			}
	    		}
	    		
	    		KeyValueAPI.put(S.TEAM, S.PASS, username+"+"+S.RECENT, latitude+"+"+longitude+" " + recent);
	    		
	    	}
	    	catch(Exception e)
	    	{
	    		Toast.makeText(MenuScreen.this, "Cannot track your location", Toast.LENGTH_SHORT).show();
	    	}
    	}
    	
    }
    
    
    public double radius(double lat1, double lon1, double lat2, double lon2)
    {
    	double R = 6371; 
    	double dLat = Math.toRadians(lat2-lat1);
    	double dLon = Math.toRadians(lon2-lon1);
    	lat1 = Math.toRadians(lat1);
    	lat2 = Math.toRadians(lat2);

    	double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    	        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    	double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    	
    	return  R * c; 
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.u_menu_screen, menu);
        return true;
    }


	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}


	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}


	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
    
	
	public void detectMessages()
	{
		//check for latlong within 100 meters and store index i
		//iterate through msg_queue with same the same value if i
		//read the whole message and send a notification
		// now add this to sent users notification that the message is recieved
		if((latitude != 0) && (latitude != prevLatitude))
		{
			try
			{
				
				String latandlong = KeyValueAPI.get(S.TEAM, S.PASS, S.LATLONG_QUEUE);
				Scanner scan = new Scanner(latandlong);
				String currentLat = "";
				String currentLon = "";
				int count = 0;
				
				while(scan.hasNext())
	    		{
	    			char[] latlong = scan.next().toCharArray();
	    			boolean divide = false;
	    			for(int i = 0; i < latlong.length; i++)
	    			{
	    				if(latlong[i] == '+')
	    				{
	    					divide = true;
	    				}
	    				else if(!divide)
	    				{
	    					currentLat += latlong[i];
	    				}
	    				else if(divide)
	    				{
	    					currentLon += latlong[i];
	    				}
	    			}
	    			
	  
	    			if(radius(latitude, longitude, Double.valueOf(currentLat), Double.valueOf(currentLon)) < 0.2)
	    			{
	    				messageDetected(count);
	    				deleteFromQueue(latandlong,currentLat+"+"+currentLon,username+S.LATLONG_QUEUE);
	    				//rememember count and send notification and delete from both msg_queue and latlongqueue and download, upload it to users inbox;	
	    			}
	    			count++;
	    		}
	    		
			}
			catch(Exception e)
			{
				
			}
		}
		
	
	}
	
	public void messageDetected(int count)
	{
		try
		{
			String msgQueue = KeyValueAPI.get(S.TEAM, S.PASS, username+S.MSG_QUEUE);
			Scanner scan = new Scanner(msgQueue);
			String completeMessage = "";
			String from = "";
			String location = "";
			String message = "";
			String type = "";
			String date = "";
			String time = "";
			
			for(int i=0;i<=count;i++)
			{
				completeMessage = scan.next();
			}
			
			// the completemessage should be parsed
			scan = new Scanner(completeMessage).useDelimiter("|");
		    from = scan.next();
		    location = scan.next();
		    message = scan.next();
		    type = scan.next();
		    date = scan.next();
		    time = scan.next();
			
		    
			//download and edit the file and upload
		    String fileUrl = UploadAPI.download(S.TEAM, S.PASS, username+S.INBOX);
		    String filename = fileUrl.substring(fileUrl.lastIndexOf('/') + 1, fileUrl.length());
		    String downloadResult = HttpDownloader.downFile(fileUrl, "", filename);
		    
		    
		    File sdcard = Environment.getExternalStorageDirectory();
	        
	        File file = new File(sdcard.getAbsolutePath()+"/"+ username+S.INBOX);

		    BufferedWriter writer = new BufferedWriter(new FileWriter(file,true));
		    writer.newLine();
		    writer.write(completeMessage.replace("|", " "));
		    
		    UploadAPI.uploadFile(S.TEAM, S.PASS, username+S.INBOX, "");
		    
		    file.delete();
		    
		    //notificationn sudhmayi nanduri
			
		    deleteFromQueue(msgQueue,completeMessage, username+S.MSG_QUEUE);
		}
		catch(Exception e)
		{
			
		}
	}
	
	public void deleteFromQueue(String queue, String msg, String key)
	{
		try
		{
			queue = queue.replace(msg, " ");
			
			KeyValueAPI.put(S.TEAM, S.PASS, key, queue);
		}
		catch(Exception e)
		{
			
		}
	}
	
	public void directNotifications()
	{
		//have a directnotifications key and always check for it,, where users receive direct messages
		// from users abt timer messages and recived messages
	}
    
}