package com.example.umessenger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import edu.neu.mobileclass.apis.KeyValueAPI;
import edu.neu.mobileclass.apis.UploadAPI;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewUser extends Activity {

	private EditText desiredUsernametext;
	private EditText passwordtext;
	private Button submit;
	private String desiredUsername;
	private String password;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_new_user);
        
        desiredUsernametext = ((EditText) findViewById(R.id.d_username_edit));
        passwordtext = ((EditText) findViewById(R.id.d_password_edit));
        submit = (Button) findViewById(R.id.d_button_submit);
        
        submit.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) 
			{
				
				desiredUsername = desiredUsernametext.getText().toString();
				password = passwordtext.getText().toString();
			   	try
			   	{
			   		String records = KeyValueAPI.get(S.TEAM, S.PASS, S.USER_RECORDS);
			   		
			   		if(desiredUsername.length() < 3 || password.length() < 3)
			   		{
			   			alertBuilder("username and password must be more than 3 characters");
			   	    }
			   		else if(desiredUsername.length() > 8 || password.length() > 8)
			   		{
			   			alertBuilder("username must be less than 8 characters");
			   		}
			   		else if(desiredUsername.contains(" ") || password.contains(" "))
			   		{
			   			alertBuilder("No spaces allowed");
			   		}
			   		else if(desiredUsername.contains("[0-9]*"))
			   		{
			   			alertBuilder("[0-9]*");
			   		}
			   		else if(records.contains(" "+desiredUsername+"+"))
			   		{
			   		     alertBuilder("username already in use");				   		
			   		}
			   		else 
			   		{
			   		    KeyValueAPI.put(S.TEAM, S.PASS, S.USER_RECORDS, records + desiredUsername+"+"+password+" ");	
			   		    
			   		    
			   		    if(createFile(desiredUsername))
			   		    {
			   		      Toast.makeText(NewUser.this, "Succesfully Registered, Welcome to uMessanger", Toast.LENGTH_SHORT).show();
			   		      Intent intent = new Intent(NewUser.this, MenuScreen.class);
			   		      intent.putExtra("username", desiredUsername);
			   		      startActivity(intent);
			   		    }
			   		    else
			   		    {
			   		    	Toast.makeText(NewUser.this, "Service Unavailable", Toast.LENGTH_SHORT).show();
			   		    }
			   		}
			   		
			   	}
			   	catch(Exception e)
			   	{
			   		Toast.makeText(NewUser.this, "Service Unavailable", Toast.LENGTH_SHORT).show();
			   	}
			}
		});
    }

    
    public boolean createFile(String username)
    {
    	
    	try
    	{

	        if(!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
	            Toast.makeText(this, "External SD card not mounted", Toast.LENGTH_LONG).show();
	            return false;
	        }
	        
    	     KeyValueAPI.put(S.TEAM, S.PASS, username+S.FRIENDS, " ");
    	     KeyValueAPI.put(S.TEAM, S.PASS, username+S.IN_REQUESTS, " ");
    	     KeyValueAPI.put(S.TEAM, S.PASS, username+S.OUT_REQUESTS, "");
    	     KeyValueAPI.put(S.TEAM, S.PASS, username+S.INBOX, "");
    	     KeyValueAPI.put(S.TEAM, S.PASS, username+S.LATLONG_QUEUE, "");
    	     KeyValueAPI.put(S.TEAM, S.PASS, username+S.MSG, "");
    	     KeyValueAPI.put(S.TEAM, S.PASS, username+S.MSG_QUEUE, "");
    	     KeyValueAPI.put(S.TEAM, S.PASS, username+S.RECENT, "");
    	     
    	     
    	   //create file usernameinbox with welcome message
    	     File sdcard = Environment.getExternalStorageDirectory();
    	        
    	     File file = new File(sdcard.getAbsolutePath()+"/"+ username+S.INBOX+".txt");
    	     file.createNewFile();
		
        	 BufferedWriter buf = new BufferedWriter(new FileWriter(file, true)); 
             buf.write("vikranth 41.249430+-71.1009 Hello_Madam_how_are_you_doing text date time no no");
             buf.close();
                
             String result = UploadAPI.uploadFile(S.TEAM, S.PASS, username+S.INBOX, sdcard.getAbsolutePath()+"/"+ username+S.INBOX+".txt");
             
             Toast.makeText(NewUser.this, result, Toast.LENGTH_SHORT).show();
             if(result.contains("uploaded"))
             {
            	 return true;
             }
             else
             {
            	 return false;
             }
    	}
    	catch(Exception e)
    	{
    		
    	}
    	
    	return false;
    	
    }
    
    public void alertBuilder(String message)
    {
    	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewUser.this);

		alertDialogBuilder
				.setMessage(message
						).setPositiveButton(
						"OK",
						new DialogInterface.OnClickListener() {

							public void onClick(
									DialogInterface dialog,
									int which) {

							}
						});
		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();	
	
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.u_new_user, menu);
        return true;
    }
}
