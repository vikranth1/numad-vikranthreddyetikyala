package com.example.umessenger;

public interface S 
{
	
	final String TEAM = "umessenger";
	final String PASS = "123456";
	final String USER_RECORDS = "userrecords";
	final String RECENT = "recent";
	final String MSG = "msg";
	final String FRIENDS = "friends";
	final String IN_REQUESTS = "incoming requests";
	final String OUT_REQUESTS = "outgoing_requests";
	final String INBOX = "inbox";
	final String LATLONG_QUEUE = "latlongqueue";
	final String MSG_QUEUE = "msgqueue";

}
