package com.example.umessenger;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FriendMenu extends Activity {

	private Button manage;
	private Button friendreq;
	private Button addfriend;
	private String username;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_friend_menu);
        
        manage = (Button) findViewById(R.id.button_manage_friends);
        friendreq = (Button) findViewById(R.id.button_friend_req);
        addfriend = (Button) findViewById(R.id.button_add_friend);
        username = getIntent().getExtras().getString("username");
        
        manage.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Intent intent = new Intent(FriendMenu.this,Friends.class);
				intent.putExtra("username", username);
				startActivity(intent);
				
			}
		});
        
        friendreq.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Intent intent = new Intent(FriendMenu.this, FriendRequests.class);
				intent.putExtra("username", username);
				startActivity(intent);
				
			}
		});
        
        addfriend.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(FriendMenu.this, AddFriend.class);
				intent.putExtra("username", username);
				startActivity(intent);
				
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.u_friend_menu, menu);
        return true;
    }
}
