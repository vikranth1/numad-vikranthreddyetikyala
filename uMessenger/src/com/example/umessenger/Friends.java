package com.example.umessenger;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.neu.mobileclass.apis.KeyValueAPI;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Friends extends ListActivity {

	private List<String> friends;
	private String username;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        friends = new ArrayList<String>();
        username = getIntent().getExtras().getString("username");
       
        
        try
        {
        	String friend = KeyValueAPI.get(S.TEAM, S.PASS, username+S.FRIENDS);
        	
        	Scanner scan = new Scanner(friend);
        	
        	while(scan.hasNext())
        	{
        		friends.add(scan.next());
        	}
        }
        catch(Exception e)
        {
        	Toast.makeText(Friends.this, "Service Not Available", Toast.LENGTH_SHORT).show();
        }
        
        setListAdapter(new ArrayAdapter<String>(Friends.this, R.layout.u_friendreq_row, friends));
    }

    public void onListItemClick(ListView l, View v, int position, long id)
    {
    	Intent intent = new Intent(Friends.this, ManageFriends.class);
    	intent.putExtra("username", username);
    	intent.putExtra("friend", friends.get(position));
    	startActivity(intent);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.u_friends, menu);
        return true;
    }
}
