package com.example.umessenger;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginScreen extends Activity {

	
	private EditText username;
	private EditText password;
	private Button signin;
	private Button newuser;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.u_login_screen);
        
        username = (EditText) findViewById(R.id.username_edit);
        password = (EditText) findViewById(R.id.password_edit);
        signin = (Button) findViewById(R.id.button_submit);
        newuser = (Button) findViewById(R.id.button_newuser);
        
        
        signin.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) 
			{
				
				if(isServerAvailable())
		        {
		        	if(authenticate())
		        	{
		        		Intent intent = new Intent(LoginScreen.this,MenuScreen.class);
		        		intent.putExtra("username", username.getText().toString());
		        		startActivity(intent);
		        		
		        	}
		        	else
		        	{
		        		Toast.makeText(LoginScreen.this, "Invalid Credetials", Toast.LENGTH_SHORT).show();		
		        	}
		        }
		        else
		        {
		        	Toast.makeText(LoginScreen.this, "Service Unavailable", Toast.LENGTH_SHORT).show();
		        }

			}
		});
        
        newuser.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) 
			{
				
			    Intent intent = new Intent(LoginScreen.this, NewUser.class);
			    startActivity(intent);
			}
		});
                
    }

    
    public boolean isServerAvailable()
    {
       try
       {
    	   if(KeyValueAPI.isServerAvailable())
    		   return true;
           
       }
       catch(Exception e)
       {
    	   Toast.makeText(LoginScreen.this, "No Internet", Toast.LENGTH_SHORT).show();
       }
       
       return false;
          
    }
    
    public boolean authenticate()
    {
    	
    	 String user = username.getText().toString();
    	 String pass = password.getText().toString();
       
    	 if(user.equals("") || pass.equals(""))
    	 {
    		 return false;
    	 }
    	 
    	 try
    	 {
    		 String records = KeyValueAPI.get(S.TEAM, S.PASS, S.USER_RECORDS);
    		 
    		 if(records.contains(" "+user+"+"+pass+" "))
    			 return true;
    		 else
    			 return false;
    		  
    	 }
    	 catch(Exception e)
    	 {
    		 return false;
    	 }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.u_login_screen, menu);
        return true;
    }
}
